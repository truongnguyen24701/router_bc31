import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class NavigateHeader extends Component {
    render() {
        return (
            <div className='h-20 flex justify-between bg-blue-400 items-center text-2xl px-10 text-white'>
                <NavLink to="/" exact activeClassName='text-red-500'>
                    Home
                </NavLink>

                <NavLink to="/movies" exact activeClassName='text-red-500'>
                    Movie List
                </NavLink>

                <NavLink to="/demo-hook" exact activeClassName='text-red-500'>
                    Demo Hook
                </NavLink>

                <NavLink to="/login" activeClassName='text-red-500'>
                    Login
                </NavLink>
                
                <NavLink to="/ex-car" activeClassName='text-red-500'>
                    Ex Car
                </NavLink>
            </div >
        )
    }
}
