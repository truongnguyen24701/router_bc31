import React, { useState } from 'react'
import { useCallback } from 'react'
import { useEffect } from 'react'
import DemoHeaderHook from './DemoHeaderHook'
import { useMemo } from 'react'


export default function DemoLifeCycleHook() {
    const [like, setLike] = useState(0)
    const [share, setShare] = useState(0)
    const [scoreArr, setScoreArr] = useState([1, 2, 3, 4])

    useEffect(() => {
        console.log("yes useEffect");
    }, [like, share])

    // ghi nhớ function
    const handlePlusLike = useCallback(() => {
        setLike(like + 1)
    }, [like])

    // userMemo: ghi nhớ kết quả tính toán
    let totalScore = useMemo(() => {
        return scoreArr.reduce((pre, current) => {
            return pre + current
        }, 0)
    }, [])

    return (
        <div className='text-center'>
            <h2>DemoLifeCycleHook  </h2>

            <DemoHeaderHook handlePlusLike={handlePlusLike} like={like} />

            <div className='mb-4'>
                <span className='text-2xl mx-10'>{like}</span>
                <button
                    onClick={() => {
                        setLike(like + 1)
                    }}
                    className='px-5 py-2 rounded bg-red-400'>Plus Like</button>
            </div>

            <div>
                <span className='text-2xl mx-10'>{share}</span>
                <button
                    onClick={() => {
                        setShare(share + 1)
                    }}
                    className='px-5 py-2 rounded bg-blue-400'>Plus Share</button>
            </div>

        </div>
    )
}
