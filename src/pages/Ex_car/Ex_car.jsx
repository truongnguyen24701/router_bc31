import React from 'react'
import { useState } from 'react'

let colorArr = ['red', 'black', 'green', 'blue', 'violet']

export default function Ex_car() {

  const [carColor, setCarColor] = useState("red")

  // let carBg = `bg-${carColor}-500`

  return (
    <div className='p-10'>
      <div className='flex'>
        <div
          style={{ background: carColor }}
          // className={`w-40 h-40 ${carBg}`}
          className="w-40 h-40">
        </div>

        <div className='space-x-5'>
          {colorArr.map((item) => {
            return (
              <button
                onClick={() => {
                  setCarColor(item)
                }}
                style={{ backgroundColor: item }}
                className='rounded text-white px-5 py-2 ml-20'>
                {item}</button>
            )
          })}
        </div>
      </div>
    </div>
  )
}
